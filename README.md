# btg-validjsonq

#### 项目介绍
> 基于validq使用json格式配置对json数据进行格式校验。

#### 使用说明
```
        String dataStr = "{\n" +
                "    \"type_number\": null,\n" +
                "    \"type_boolean\": true,\n" +
                "    \"type_string\": \"abcd\",\n" +
                "    \"type_object\": {\n" +
                "      \"o1_1\": null,\n" +
                "      \"o1_2\": \"abcd\"\n" +
                "    },\n" +
                "    \"type_list\": [\n" +
                "      {\n" +
                "        \"o1_1\": null,\n" +
                "        \"o1_2\": \"abcd\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"o1_1\": 123,\n" +
                "        \"o1_2\": \"abcd\"\n" +
                "      }\n" +
                "    ],\n" +
                "    \"type_array_string\": [\n" +
                "      null,\n" +
                "      \"abcd\"\n" +
                "    ],\n" +
                "    \"type_array_object\": [\n" +
                "      {\n" +
                "        \"o1_1\": null,\n" +
                "        \"o1_2\": \"abcd\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"o1_1\": 123,\n" +
                "        \"o1_2\": \"abcd\"\n" +
                "      }\n" +
                "    ],\n" +
                "    \"type_array_array\": [\n" +
                "      [\n" +
                "        \"abcd\",\n" +
                "        \"abcd\"\n" +
                "      ],\n" +
                "      [\n" +
                "        null,\n" +
                "        \"abcd\"\n" +
                "      ]\n" +
                "    ]\n" +
                "  }";

        JSONObject data = JSONObject.parseObject(dataStr);

        ValidJsonQCache.me.setCacheLoader(new DefaultCacheLoader());

        ValidationResult result = ValidJsonQ.create() //创建验证器
                .failContinue() //验证失败时继续下一个验证
                .configCache("demo") //设置配置缓存名称，也可直接设置一个json配置
                //.group("biz") //设置验证分组，不设置验证所有

                .setAttr("param1", "param1_value")
                .setAttr("param2", "param2_value")

                .validObject(data); //验证Object，类似由validArray、validList

        if (result.isFailure()) {//验证失败，setSuccess()可获取是否验证成功
            System.out.println(result.getFirstError().getErrorMsg()); //getFirstError()为获取第一条错误信息
        }

        for (ValidationError validationError : result.getErrors()) {//getErrors()可获取验证结果
            System.out.println(validationError);
        }

        ValidationResult result2 = ValidJsonQ.create() //创建验证器
                .failContinue() //验证失败时继续下一个验证
                .configCache("demo") //设置配置缓存名称，也可直接设置一个json配置
                .group("biz") //设置验证分组，不设置验证所有

                .setAttr("param1", "param1_value")
                .setAttr("param2", "param2_value")

                .validObject(data); //验证Object，类似由validArray、validList

        if (result2.isFailure()) {//验证失败，setSuccess()可获取是否验证成功
            System.out.println(result2.getFirstError().getErrorMsg()); //getFirstError()为获取第一条错误信息
        }

        for (ValidationError validationError : result2.getErrors()) {//getErrors()可获取验证结果
            System.out.println(validationError);
        }
```

#### 升级记录
```
4.0.5
1、btg-util升级至4.0.24；
2、自定义msg默认携带${ctx.fieldName}；

4.0.4
1、Cache加载时，允许为空；

4.0.3
1、性能优化，修复每次验证都加载缓存的问题；

4.0.2
1、优化代码组织方式；

v4.0.1
1、转为独立maven依赖，去掉parent；

v3.0.20
1、btg-parent升级到v2.0.1；

v3.0.19
1、增加上下文参数target

v3.0.18
1、缓存管理器增加手动加载方法

v3.0.17
1、自定义msg支持使用环境参数field、shortField、fieldName、shortFieldName

v3.0.16
1、修复当字段为复杂类型时，若不包含valid字段，下级验证失效的bug

v3.0.15
1、修复当target为null时，被转换为"null"的bug

v3.0.14
1、支持level+index验证优先级

v3.0.13
1、调整下标展示策略

v3.0.12
1、修复验证器顺序无效的bug

v3.0.11
1、增加对验证优先级的支持

v3.0.10
1、修复缓存再dev模式下加载最新文件提示已存在的bug

v3.0.9
1、修复验证对象为null时验证无效的bug

v3.0.8
1、支持自定义错误信息占位符使用上下文参数替换

v3.0.7
1、支持自定义错误信息

v3.0.6
1、增加开发模式
2、增加常用加载器
3、简化类命名

v3.0.5
1、增加对分组验证的支持

v3.0.4
1、增加json缓存器

v3.0.3
1、统一依赖管理

v3.0.2
1、优化验证器应用规则，缩小白名单范围；

v3.0.1
1、统一迁移至公司名下；

v1.0.1：
1、基于validq使用json格式配置对json数据进行格式校验
```
---
豆圆
