package cn.zcltd.btg.validjsonq;

import cn.zcltd.btg.validjsonq.cache.DefaultCacheLoader;
import cn.zcltd.btg.validjsonq.cache.ValidJsonQCache;
import cn.zcltd.btg.validq.ValidationError;
import cn.zcltd.btg.validq.ValidationResult;
import com.alibaba.fastjson.JSONObject;
import org.junit.Test;

/**
 * ValidJsonQ Tester.
 */
public class ValidJsonQTest {

    /**
     * Method: valid()
     */
    @Test
    public void testValid() throws Exception {
        String dataStr = "{\n" +
                "    \"type_number\": null,\n" +
                "    \"type_boolean\": true,\n" +
                "    \"type_string\": \"abcd\",\n" +
                "    \"type_object\": {\n" +
                "      \"o1_1\": null,\n" +
                "      \"o1_2\": \"abcd\"\n" +
                "    },\n" +
                "    \"type_list\": [\n" +
                "      {\n" +
                "        \"o1_1\": null,\n" +
                "        \"o1_2\": \"abcd\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"o1_1\": 123,\n" +
                "        \"o1_2\": \"abcd\"\n" +
                "      }\n" +
                "    ],\n" +
                "    \"type_array_string\": [\n" +
                "      null,\n" +
                "      \"abcd\"\n" +
                "    ],\n" +
                "    \"type_array_object\": [\n" +
                "      {\n" +
                "        \"o1_1\": null,\n" +
                "        \"o1_2\": \"abcd\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"o1_1\": 123,\n" +
                "        \"o1_2\": \"abcd\"\n" +
                "      }\n" +
                "    ],\n" +
                "    \"type_array_array\": [\n" +
                "      [\n" +
                "        \"abcd\",\n" +
                "        \"abcd\"\n" +
                "      ],\n" +
                "      [\n" +
                "        null,\n" +
                "        \"abcd\"\n" +
                "      ]\n" +
                "    ]\n" +
                "  }";

        JSONObject data = JSONObject.parseObject(dataStr);

        ValidJsonQCache.me.setCacheLoader(new DefaultCacheLoader());

        ValidationResult result = ValidJsonQ.create() //创建验证器
                .failContinue() //验证失败时继续下一个验证
                .configCache("demo") //设置配置缓存名称，也可直接设置一个json配置
                //.group("biz") //设置验证分组，不设置验证所有

                .setAttr("param1", "param1_value")
                .setAttr("param2", "param2_value")

                .validObject(data); //验证Object，类似由validArray、validList

        if (result.isFailure()) {//验证失败，setSuccess()可获取是否验证成功
            System.out.println(result.getFirstError().getErrorMsg()); //getFirstError()为获取第一条错误信息
        }

        for (ValidationError validationError : result.getErrors()) {//getErrors()可获取验证结果
            System.out.println(validationError);
        }

        ValidationResult result2 = ValidJsonQ.create() //创建验证器
                .failContinue() //验证失败时继续下一个验证
                .configCache("demo") //设置配置缓存名称，也可直接设置一个json配置
                .group("biz") //设置验证分组，不设置验证所有

                .setAttr("param1", "param1_value")
                .setAttr("param2", "param2_value")

                .validObject(data); //验证Object，类似由validArray、validList

        if (result2.isFailure()) {//验证失败，setSuccess()可获取是否验证成功
            System.out.println(result2.getFirstError().getErrorMsg()); //getFirstError()为获取第一条错误信息
        }

        for (ValidationError validationError : result2.getErrors()) {//getErrors()可获取验证结果
            System.out.println(validationError);
        }
    }
}