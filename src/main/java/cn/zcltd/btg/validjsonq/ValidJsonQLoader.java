package cn.zcltd.btg.validjsonq;

import cn.zcltd.btg.validq.ValidQ;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * 基于json规则验证的装载器
 */
public interface ValidJsonQLoader {

    /**
     * 装载object
     *
     * @param validQ          ValidQ验证工具
     * @param group           验证分组
     * @param config          json配置
     * @param data            json待验证数据
     * @param parentField     上级字段
     * @param parentFieldName 上级字段名称
     * @param level           验证层级（数字越小优先级越高，从小到大验证）
     */
    public void loadObject(ValidQ validQ, String group, JSONObject config, JSONObject data, String parentField, String parentFieldName, Integer level);

    /**
     * 装载
     *
     * @param validQ          ValidQ验证工具
     * @param group           验证分组
     * @param config          json配置
     * @param data            json待验证数据
     * @param parentField     上级字段
     * @param parentFieldName 上级字段名称
     * @param level           验证层级（数字越小优先级越高，从小到大验证）
     */
    public void loadList(ValidQ validQ, String group, JSONObject config, JSONArray data, String parentField, String parentFieldName, Integer level);

    /**
     * 装载
     *
     * @param validQ          ValidQ验证工具
     * @param group           验证分组
     * @param config          json配置
     * @param data            json待验证数据
     * @param parentField     上级字段
     * @param parentFieldName 上级字段名称
     * @param level           验证层级（数字越小优先级越高，从小到大验证）
     */
    public void loadArray(ValidQ validQ, String group, JSONObject config, JSONArray data, String parentField, String parentFieldName, Integer level);
}