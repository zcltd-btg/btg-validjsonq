package cn.zcltd.btg.validjsonq.loader;

import cn.zcltd.btg.validjsonq.ValidQLoader;
import cn.zcltd.btg.validq.ValidQ;
import cn.zcltd.btg.validq.validator.StringRegexValidator;
import com.alibaba.fastjson.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * loader:验证String是否符合正则匹配
 */
public class StringRegexLoader implements ValidQLoader {

    @Override
    public void load(ValidQ validQ, JSONObject validationConfig, Object target,
                     String attrField, String attrShortField, String attrFieldName, String attrShortFieldName,
                     Integer level, Integer index) {
        Map<String, String> defaultParams = new HashMap<>();
        defaultParams.put("ctx.field", attrField);
        defaultParams.put("ctx.shortField", attrShortField);
        defaultParams.put("ctx.fieldName", attrFieldName);
        defaultParams.put("ctx.shortFieldName", attrShortFieldName);
        defaultParams.put("ctx.target", String.valueOf(target));

        StringRegexValidator validator = new StringRegexValidator();
        validator.setField(attrField);
        validator.setShortField(attrShortField);
        validator.setFieldName(attrFieldName);
        validator.setShortFieldName(attrShortFieldName);

        String targetData = null == target ? null : String.valueOf(target);
        String msg = validationConfig.getString("msg");
        String regex = validationConfig.getString("regex");

        if (null != msg && msg.trim().length() > 0) {
            msg = "${ctx.fieldName}" + msg;
            validator.setErrorMsg(validQ.getContext().replaceParams(msg, defaultParams));
        }
        validator.setRegex(regex);

        index = null == index ? validator.index() : index;
        validQ.on(targetData, validator, level, index);
    }
}