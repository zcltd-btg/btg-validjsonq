package cn.zcltd.btg.validjsonq;

/**
 * 数据类型
 */
public enum DataType {
    STRING("string"),
    OBJECT("object"),
    LIST("list"),
    ARRAY("array");

    private String type;

    DataType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public static DataType parse(String type) {
        DataType[] items = DataType.values();
        for (DataType item : items) {
            if (item.getType().equalsIgnoreCase(type)) {
                return item;
            }
        }
        return null;
    }
}