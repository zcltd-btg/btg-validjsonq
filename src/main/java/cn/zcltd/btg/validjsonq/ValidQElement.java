package cn.zcltd.btg.validjsonq;

/**
 * 基于json规则验证的验证节点
 */
public class ValidQElement {
    private String className; //验证器类名称
    private String name; //验证器名称
    private ValidQLoader validatorLoader; //验证规则加载器

    public ValidQElement() {
    }

    public ValidQElement(String className, String name, ValidQLoader validatorLoader) {
        this.className = className;
        this.name = name;
        this.validatorLoader = validatorLoader;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ValidQLoader getValidatorLoader() {
        return validatorLoader;
    }

    public void setValidatorLoader(ValidQLoader validatorLoader) {
        this.validatorLoader = validatorLoader;
    }
}