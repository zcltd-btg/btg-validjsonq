package cn.zcltd.btg.validjsonq;

import cn.zcltd.btg.validjsonq.exception.ValidJsonQException;
import cn.zcltd.btg.validjsonq.loader.*;
import cn.zcltd.btg.validq.Validator;
import cn.zcltd.btg.validq.validator.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 基于json规则验证的验证器映射器
 */
public final class ValidQMapper {
    public static final ValidQMapper me = new ValidQMapper();//单例

    private final Map<String, ValidQElement> classMapperMap = new HashMap<>();//map结构
    private final Map<String, ValidQElement> nameMapperMap = new HashMap<>();//map结构
    private final List<ValidQElement> mapperList = new ArrayList<>();//list结构

    private ValidQMapper() {
        regist(IsNullValidator.class, "isNull", new IsNullLoader());
        regist(IsNotNullValidator.class, "isNotNull", new IsNotNullLoader());
        regist(IsEmptyValidator.class, "isEmpty", new IsEmptyLoader());
        regist(IsNotEmptyValidator.class, "isNotEmpty", new IsNotEmptyLoader());
        regist(StringLengthValidator.class, "stringLength", new StringLengthLoader());
        regist(StringRegexValidator.class, "stringRegex", new StringRegexLoader());
    }

    /**
     * 注册验证器
     *
     * @param validatorClass 验证器类
     * @param name           名称
     * @param validQLoader   加载器
     */
    public void regist(Class<? extends Validator> validatorClass, String name, ValidQLoader validQLoader) {
        String className = validatorClass.getCanonicalName();
        if (classMapperMap.containsKey(className)) {
            throw new ValidJsonQException("验证器类已被注册," + className);
        }
        if (nameMapperMap.containsKey(name)) {
            throw new ValidJsonQException("验证器名称已被注册," + name);
        }
        ValidQElement element = new ValidQElement(className, name, validQLoader);
        classMapperMap.put(className, element);
        nameMapperMap.put(name, element);
        mapperList.add(element);
    }

    /**
     * 获取一个注册对象
     *
     * @param validatorClass 验证类
     * @return 注册对象
     */
    public ValidQElement get(Class<? extends Validator> validatorClass) {
        String className = validatorClass.getCanonicalName();
        ValidQElement validQElement = classMapperMap.get(className);
        if (null == validQElement) {
            throw new ValidJsonQException("未获取到验证器," + className);
        }
        return validQElement;
    }

    /**
     * 获取一个注册对象
     *
     * @param validatorName 验证器名称
     * @return 注册对象
     */
    public ValidQElement get(String validatorName) {
        ValidQElement validQElement = nameMapperMap.get(validatorName);
        if (null == validQElement) {
            throw new ValidJsonQException("未获取到验证器," + validatorName);
        }
        return validQElement;
    }

    public Map<String, ValidQElement> getClassMapperMap() {
        return classMapperMap;
    }

    public Map<String, ValidQElement> getNameMapperMap() {
        return nameMapperMap;
    }

    public List<ValidQElement> getMapperList() {
        return mapperList;
    }
}