package cn.zcltd.btg.validjsonq.cache;

/**
 * 缓存加载器
 */
public interface CacheLoader {

    /**
     * 加载
     *
     * @param validJsonQCache 缓存
     */
    void load(ValidJsonQCache validJsonQCache);
}