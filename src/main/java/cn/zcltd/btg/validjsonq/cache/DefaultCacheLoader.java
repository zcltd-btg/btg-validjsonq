package cn.zcltd.btg.validjsonq.cache;

import cn.zcltd.btg.sutil.FileUtil;
import cn.zcltd.btg.sutil.PathUtil;
import cn.zcltd.btg.sutil.filescanner.FileScanner;
import com.alibaba.fastjson.JSONObject;

import java.io.File;
import java.util.List;

/**
 * 默认缓存加载器
 * 将制定路径下的.json文件加载到
 */
public class DefaultCacheLoader implements CacheLoader {

    private String path;//加载路径

    public DefaultCacheLoader(String path) {
        this.path = path;
    }

    public DefaultCacheLoader() {
        this.path = PathUtil.getRootClassPath();
    }

    @Override
    public void load(ValidJsonQCache validJsonQCache) {
        List<File> files = FileScanner.findFiles(path, ".*\\.vjq\\.json");
        for (File file : files) {
            validJsonQCache.add(file.getName().replace(".vjq.json", ""), JSONObject.parseObject(FileUtil.readToString(file)));
        }
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}