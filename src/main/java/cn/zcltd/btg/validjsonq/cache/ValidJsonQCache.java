package cn.zcltd.btg.validjsonq.cache;

import cn.zcltd.btg.sutil.EmptyUtil;
import com.alibaba.fastjson.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * json缓存器
 */
public class ValidJsonQCache {
    private CacheLoader cacheLoader;
    private Map<String, JSONObject> cache = new HashMap<>();
    private boolean devMode = false;

    private boolean initCacheLoder = false;
    public static final ValidJsonQCache me = new ValidJsonQCache();

    private ValidJsonQCache() {
    }

    /**
     * 获取缓存
     *
     * @param name 名称
     * @return 缓存
     */
    public JSONObject get(String name) {
        if (!initCacheLoder) {
            throw new RuntimeException("尚未初始化CacheLoader，请使用setCacheLoader初始化");
        }
        if (devMode) {
            cacheLoader.load(this);
        }
        if (!cache.containsKey(name)) {
            throw new RuntimeException("name not exists," + name);
        }
        return cache.get(name);
    }

    /**
     * 添加缓存
     *
     * @param name 名称
     * @param json josn
     */
    public void add(String name, JSONObject json) {
        if (!devMode && cache.containsKey(name)) {
            throw new RuntimeException("name is exists," + name);
        }
        if (EmptyUtil.isEmpty(json)) {
//            throw new RuntimeException("json can not be empty");
            json = new JSONObject();
        }
        cache.put(name, json);
    }

    /**
     * 移除缓存
     *
     * @param name 名称
     */
    public void remove(String name) {
        if (!cache.containsKey(name)) {
            throw new RuntimeException("name not exists," + name);
        }
        cache.remove(name);
    }

    /**
     * 从加载器加载缓存
     */
    public void load() {
        if (!initCacheLoder) {
            throw new RuntimeException("尚未初始化CacheLoader，请使用setCacheLoader初始化");
        }
        this.cacheLoader.load(this);
    }

    public CacheLoader getCacheLoader() {
        return cacheLoader;
    }

    public ValidJsonQCache setCacheLoader(CacheLoader cacheLoader) {
        this.cacheLoader = cacheLoader;
        cacheLoader.load(this);
        initCacheLoder = true;
        return this;
    }

    public Map<String, JSONObject> getCache() {
        return cache;
    }

    public ValidJsonQCache setCache(Map<String, JSONObject> cache) {
        this.cache = cache;
        return this;
    }

    public boolean isDevMode() {
        return devMode;
    }

    public ValidJsonQCache setDevMode(boolean devMode) {
        this.devMode = devMode;
        return this;
    }
}