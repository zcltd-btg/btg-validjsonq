package cn.zcltd.btg.validjsonq;

import cn.zcltd.btg.validq.ValidQ;
import com.alibaba.fastjson.JSONObject;

/**
 * 基于json规则验证的验证器装载器
 */
public interface ValidQLoader {

    /**
     * 将验证器加载到validq
     *
     * @param validQ             validq
     * @param validationConfig   验证规则
     * @param target             待验证数据
     * @param attrField          字段
     * @param attrShortField     短字段
     * @param attrFieldName      字段名称
     * @param attrShortFieldName 短字段名称
     * @param level              验证层级（数字越小优先级越高，从小打到进行验证）
     * @param index              验证优先级（数字越小优先级越高，从小打到进行验证）
     */
    public void load(ValidQ validQ, JSONObject validationConfig, Object target,
                     String attrField, String attrShortField, String attrFieldName, String attrShortFieldName,
                     Integer level, Integer index);
}